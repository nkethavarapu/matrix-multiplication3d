
function validate_matrix(matrix) {
    var [matrix_rows,matrix_cols]=matrix_size(matrix)
    for (var i = 0; i < matrix_rows; i++) {
      if (matrix[i].length != matrix[i + 1].length) {
        return false;
      } else {
        for (var j = 0; j < matrix_rows; j++) {
          for (var k = 0; k < matrix_cols; k++) {
            if (typeof matrix[j][k] !== "number") {
              return false;
            } else {
                 continue;
            }
          }
         
        }
        return true;
      }
    }
  }
    
  function matrix_size(matrix){
      var matrix_rows = matrix.length;
    var matrix_cols = matrix[0].length;
      return [matrix_rows,matrix_cols]
  }
  
  function validate_mul_matrixes(matrix1, matrix2) {
    if (validate_matrix(matrix1) && validate_matrix(matrix2)) {
      var matrix1_cols = matrix1[0].length;
      var matrix2_rows = matrix2.length;
      if (matrix1_cols != matrix2_rows) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }
  function multiplication2d(matrix1,matrix2){
      if(validate_mul_matrixes(matrix1,matrix2)){
          ;var result=[];
          var [matrix1_rows,matrix1_cols] = matrix_size(matrix1);
          var [matrix2_rows,matrix2_cols] = matrix_size(matrix2);
          
              for (var row = 0; row < matrix1_rows; row++) {
                  result[row] = []
                  for (var col = 0; col < matrix2_cols; col++) {
                      sum=0;
                      for (var k = 0; k < matrix1_cols; k++) {
                          sum += (matrix1[row][k]) * (matrix2[k][col]);
                      }
                      result[row][col] = sum;
                  }
              }
              return result;
          }
          else {
              return "not valid";
          }
  
  
      }
      function multiplication3d(mat1, mat2) {
        var result3d = [];
      
        for (var i = 0; i < mat1.length; i++) {
          var ans = multiplication2d(mat1[i], mat2[i]);
          
          result3d.push(ans);
        }
      
        return result3d;
      }
  
  
  
  var matrix1 = [
    [
      [6, 3, 7],
      [4, 6, 6],
      [1, 2, 3],
      
    ],
  
    [
      [2, 6, 7],
      [4, 3, 7],
      [1, 3, 4]
    ],
  
    [
      [7, 2, 5],
      [4, 1, 7],
      [1, 4, 3]
    ]
  ];
  
  
  var matrix2 = [
    [
      [6, 3, 7],
      [4, 6, 9],
      [1, 2, 3]
    ],
  
    [
      [2, 6, 7],
      [4, 3, 7],
      [1, 3, 4]
    ],
  
    [
      [7, 2, 5],
      [4, 1, 7],
      [1, 4, 3]
    ]
  ];
  var x =multiplication3d(matrix1,matrix2)
  console.log(x);
  